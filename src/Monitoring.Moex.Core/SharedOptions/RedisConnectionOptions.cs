﻿namespace Monitoring.Moex.Core.SharedOptions
{
    public class RedisConnectionOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
