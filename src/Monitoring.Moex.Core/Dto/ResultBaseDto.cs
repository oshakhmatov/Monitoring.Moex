﻿namespace Monitoring.Moex.Core.Dto
{
    public abstract class ResultBaseDto
    {
        public string? ErrorMessage { get; set; }

        public ResultBaseDto(string errorMessage)
        {
            ErrorMessage = errorMessage ?? throw new ArgumentNullException(nameof(errorMessage));
        }

        protected ResultBaseDto() { }
    }
}
