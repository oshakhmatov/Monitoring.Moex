﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Moex.Core.Dto.SecurityTotals
{
    public class BlueChipsResultDto : ResultBaseDto
    {
        public List<SecurityTotalShortDto>? Totals { get; set; }

        public BlueChipsResultDto(List<SecurityTotalShortDto> totals)
        {
            Totals = totals ?? throw new ArgumentNullException(nameof(totals));
        }

        public BlueChipsResultDto(string errorMessage) : base(errorMessage)
        {
        }
    }
}
