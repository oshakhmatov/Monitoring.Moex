﻿namespace Monitoring.Moex.Core.Dto.SecurityTotals
{
    public class HighestTotalResultDto : ResultBaseDto
    {
        public SecurityTotalShortDto? Total { get; set; }

        public HighestTotalResultDto(SecurityTotalShortDto total)
        {
            Total = total ?? throw new ArgumentNullException(nameof(total));
        }

        public HighestTotalResultDto(string errorMessage) : base(errorMessage)
        {
        }
    }
}
