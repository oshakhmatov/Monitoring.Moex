﻿namespace Monitoring.Moex.Core.Dto.SecurityTotals
{
    public class LastTotalsResultDto : ResultBaseDto
    {
        public string? TradeDate { get; set; }
        public List<SecurityTotalShortDto>? SecurityTotals { get; set; }

        public LastTotalsResultDto(string tradeDate, List<SecurityTotalShortDto> securityTotals)
        {
            TradeDate = tradeDate;
            SecurityTotals = securityTotals;
        }

        public LastTotalsResultDto(string errorMessage) : base(errorMessage)
        {
        }
    }
}
