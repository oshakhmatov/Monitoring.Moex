﻿using Monitoring.Moex.Core.Models;

namespace Monitoring.Moex.Core.Services.SecuritiesMonitoring
{
    public interface ISecuritiesParser
    {
        IEnumerable<Security> Parse(string input);
    }
}
