﻿namespace Monitoring.Moex.Core.Services.SecuritiesMonitoring
{
    public class SecuritiesMonitoringOptions
    {
        public string SecuritiesUrl { get; set; }
        public int Interval { get; set; }
    }
}
