﻿using Monitoring.Moex.Core.Models;

namespace Monitoring.Moex.Core.Services.LastTotalsMonitoring
{
    public interface ITotalsParser
    {
        IEnumerable<SecurityTotal> Parse(string input);
    }
}
