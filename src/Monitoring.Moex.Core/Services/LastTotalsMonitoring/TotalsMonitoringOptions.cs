﻿namespace Monitoring.Moex.Core.Services.LastTotalsMonitoring
{
    public class TotalsMonitoringOptions
    {
        public int Interval { get; set; }
        public string LastTotalsUrl { get; set; }
    }
}
