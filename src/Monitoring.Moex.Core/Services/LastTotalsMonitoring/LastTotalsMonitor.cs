﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Monitoring.Moex.Core.DataAccess.Repos;
using Monitoring.Moex.Core.Helpers;
using Monitoring.Moex.Core.Models;

namespace Monitoring.Moex.Core.Services.LastTotalsMonitoring
{
    public class LastTotalsMonitor : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IOptionsMonitor<TotalsMonitoringOptions> _totalsMonitorOptions;
        private readonly ILogger<LastTotalsMonitor> _logger;

        public LastTotalsMonitor(
            IServiceScopeFactory serviceScopeFactory,
            IOptionsMonitor<TotalsMonitoringOptions> totalsMonitorOptions,
            ILogger<LastTotalsMonitor> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _totalsMonitorOptions = totalsMonitorOptions;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var client = new HttpClient();
                var scope = _serviceScopeFactory.CreateScope();

                try
                {
                    var options = _totalsMonitorOptions.CurrentValue;

                    await Task.Delay(options.Interval, stoppingToken);

                    var content = await client.GetStringAsync(options.LastTotalsUrl, stoppingToken);

                    var totalsParser = scope.ServiceProvider.GetRequiredService<ITotalsParser>();
                    var totals = totalsParser.Parse(content).Where(s => s.IsNotEmpty());

                    var totalsRepo = scope.ServiceProvider.GetRequiredService<ITotalsRepo>();
                    var lastTradeClock = await totalsRepo.GetLastTradeClockAsync();

                    if (DataIsNotActual(lastTradeClock, totals))
                        continue;

                    var securitiesRepo = scope.ServiceProvider.GetRequiredService<ISecuritiesRepo>();
                    var securityIds = await securitiesRepo.GetAllSecurityIdsAsync();

                    var totalsToSave = totals.Where(t => securityIds.Contains(t.SecurityId!)).ToArray();

                    Parallel.ForEach(totalsToSave, (total) =>
                    {
                        total.OpenCloseDelta = GetPercentageDelta(total.Open!.Value, total.Close!.Value, digitsAfterPoint: 2);
                    });

                    await totalsRepo.AddRangeAsync(totalsToSave);

                    _logger.LogInformation($"New totals added for {Utils.GetDateString(lastTradeClock)}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
                finally
                {
                    client?.Dispose();
                    scope?.Dispose();
                }
            }
        }

        private static double GetPercentageDelta(double first, double second, int digitsAfterPoint)
        {
            var absPercentage = Math.Round(100 * (Math.Abs(first - second) / first), digitsAfterPoint);

            return first > second ? absPercentage * (-1) : absPercentage;
        }

        private static bool DataIsNotActual(long? lastTradeClock, IEnumerable<SecurityTotal> totals)
        {
            return lastTradeClock == totals.First().TradeClock;
        } 
    }
}
