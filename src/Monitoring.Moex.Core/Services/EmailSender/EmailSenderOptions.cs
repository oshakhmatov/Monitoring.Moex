﻿namespace Monitoring.Moex.Core.Services.EmailSender
{
    public class EmailSenderOptions
    {
        public int Interval { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
    }
}
