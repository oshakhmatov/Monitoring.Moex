﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Monitoring.Moex.Core.DataAccess.Repos;
using Monitoring.Moex.Core.Helpers;
using Monitoring.Moex.Core.Services.SecurityTotals;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Monitoring.Moex.Core.Services.EmailSender
{
    public class EmailSender : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IOptionsMonitor<EmailSenderOptions> _emailSenderOptions;
        private readonly ILogger<EmailSender> _logger;

        public EmailSender(
            IServiceScopeFactory serviceScopeFactory,
            IOptionsMonitor<EmailSenderOptions> emailSenderOptions,
            ILogger<EmailSender> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _emailSenderOptions = emailSenderOptions;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var options = _emailSenderOptions.CurrentValue;
                await Task.Delay(options.Interval, stoppingToken);

                var scope = _serviceScopeFactory.CreateScope();
                var smtpClient = new SmtpClient();

                try
                {
                    var emailSubscribersRepo = scope.ServiceProvider.GetRequiredService<IEmailSubcribersRepo>();
                    var emailSubscribers = await emailSubscribersRepo.ListAsync();

                    if (!emailSubscribers.Any())
                        continue;

                    var securityTotalsUnit = scope.ServiceProvider.GetRequiredService<SecurityTotalsUnit>();
                    var highestUp = await securityTotalsUnit.GetHighestUpAsync();
                    var highestDown = await securityTotalsUnit.GetHighestDownAsync();
                    var blueChips = await securityTotalsUnit.GetBlueChipsAsync();

                    var totalsRepo = scope.ServiceProvider.GetRequiredService<ITotalsRepo>();
                    var lastTradeClock = await totalsRepo.GetLastTradeClockAsync();

                    if (highestUp is null || highestDown is null || lastTradeClock is null)
                        continue;

                    var subject = $"Итоги торгов {Utils.GetDateString(lastTradeClock)}";
                    var sb = new StringBuilder();

                    sb.AppendLine("<h2>Наибольший прирост</h2>")
                        .AppendLine($"<p>{highestUp.Total.SecurityName} {highestUp.Total.DeltaPercentage}%</p>")
                        .AppendLine()
                        .AppendLine("<h2>Наибольшее падение</h2>")
                        .AppendLine($"<p>{highestDown.Total.SecurityName} {highestDown.Total.DeltaPercentage}%</p>");

                    if (blueChips.Totals.Any())
                    {
                        sb.AppendLine();
                        sb.AppendLine("<h2>Голубые фишки</h2>");
                        foreach (var blueChip in blueChips.Totals)
                            sb.AppendLine($"<p>{blueChip.SecurityName} {blueChip.DeltaPercentage}%</p>");
                    }

                    smtpClient.Host = options.Host;
                    smtpClient.EnableSsl = true;
                    smtpClient.Credentials = new NetworkCredential()
                    {
                        UserName = options.Address,
                        Password = options.Password
                    };

                    Parallel.ForEach(emailSubscribers, (subscriber) =>
                    {
                        if (lastTradeClock.Value == subscriber.LastClock)
                            return;

                        var message = new MailMessage(from: new MailAddress(options.Address), to: new MailAddress(subscriber.Email))
                        {
                            IsBodyHtml = true,
                            Subject = subject,
                            Body = sb.ToString()
                        };

                        smtpClient.Send(message);
                        subscriber.LastClock = lastTradeClock.Value;
                    });

                    await emailSubscribersRepo.UpdateRangeAsync(emailSubscribers);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
                finally
                {
                    scope?.Dispose();
                    smtpClient?.Dispose();
                }
            }
        }
    }
}
