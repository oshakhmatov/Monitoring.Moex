﻿using Monitoring.Moex.Core.DataAccess.Repos;
using Monitoring.Moex.Core.Dto.SecurityTotals;
using Monitoring.Moex.Core.Helpers;

namespace Monitoring.Moex.Core.Services.SecurityTotals
{
    public class SecurityTotalsUnit
    {
        private readonly ITotalsRepo _totalsRepo;

        public SecurityTotalsUnit(ITotalsRepo totalsRepo)
        {
            _totalsRepo = totalsRepo;
        }

        public async Task<LastTotalsResultDto> GetLastTotalsAsync()
        {
            var lastTradeClock = await _totalsRepo.GetLastTradeClockAsync();

            if (lastTradeClock is long clock)
            {
                var totals = await _totalsRepo.ListByClockAsync(clock);

                if (totals.Any())
                    return new LastTotalsResultDto(tradeDate: Utils.GetDateString(clock), securityTotals: totals);
            }

            return new LastTotalsResultDto("Нет данных");
        }

        public async Task<HighestTotalResultDto> GetHighestUpAsync()
        {
            return await GetHighestTotalAsync(getHighestUp: true);
        }

        public async Task<HighestTotalResultDto> GetHighestDownAsync()
        {
            return await GetHighestTotalAsync(getHighestUp: false);
        }

        public async Task<BlueChipsResultDto> GetBlueChipsAsync()
        {
            var lastTradeClock = await _totalsRepo.GetLastTradeClockAsync();

            if (lastTradeClock is long clock)
            {
                var totals = await _totalsRepo.ListByClockAsync(clock);

                if (totals.Any())
                {
                    return new BlueChipsResultDto(totals.Where(t =>
                    t.SecurityId == "AFLT" ||
                    t.SecurityId == "VTBR" ||
                    t.SecurityId == "GAZP" ||
                    t.SecurityId == "LKOH" ||
                    t.SecurityId == "MGNT" ||
                    t.SecurityId == "MVID" ||
                    t.SecurityId == "ROSN").ToList());
                }
            }

            return new BlueChipsResultDto("Нет данных");
        }

        private async Task<HighestTotalResultDto> GetHighestTotalAsync(bool getHighestUp)
        {
            var lastTradeClock = await _totalsRepo.GetLastTradeClockAsync();

            if (lastTradeClock is long clock)
            {
                var total = getHighestUp ?
                    await _totalsRepo.GetHighestUpByClockAsync(clock) :
                    await _totalsRepo.GetHighestDownByClockAsync(clock);

                if (total is not null)
                    return new HighestTotalResultDto(total);
            }

            return new HighestTotalResultDto("Нет данных");
        }
    }
}
