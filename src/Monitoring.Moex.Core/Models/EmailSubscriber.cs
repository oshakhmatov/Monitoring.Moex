﻿namespace Monitoring.Moex.Core.Models
{
    public class EmailSubscriber
    {
        public EmailSubscriber(string email)
        {
            Email = email;
        }

        public long Id { get; set; }
        public string Email { get; set; }
        public long LastClock { get; set; }
    }
}
