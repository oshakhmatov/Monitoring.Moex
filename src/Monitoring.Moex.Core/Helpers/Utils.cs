﻿namespace Monitoring.Moex.Core.Helpers
{
    public static class Utils
    {
        public static string GetDateString(long? clock)
        {
            return clock == null
                ? throw new ArgumentNullException(nameof(clock))
                : DateTimeOffset.FromUnixTimeSeconds(clock.Value).Date.ToShortDateString();
        }
    }
}
