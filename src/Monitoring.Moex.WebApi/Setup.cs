﻿using Microsoft.EntityFrameworkCore;
using Monitoring.Moex.Core.DataAccess;
using Monitoring.Moex.Core.Services.EmailSender;
using Monitoring.Moex.Core.Services.LastTotalsMonitoring;
using Monitoring.Moex.Core.Services.SecuritiesMonitoring;
using Monitoring.Moex.Core.SharedOptions;
using Monitoring.Moex.Infrastructure.Data;
using StackExchange.Redis;
using System.Reflection;

namespace Monitoring.Moex.WebApi
{
    public static class Setup
    {
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration config)
        {
            return services
                .Configure<DbConnectionOptions>(config.GetSection(nameof(DbConnectionOptions)))
                .Configure<RedisConnectionOptions>(config.GetSection(nameof(RedisConnectionOptions)))
                .Configure<TotalsMonitoringOptions>(config.GetSection(nameof(TotalsMonitoringOptions)))
                .Configure<SecuritiesMonitoringOptions>(config.GetSection(nameof(SecuritiesMonitoringOptions)))
                .Configure<EmailSenderOptions>(config.GetSection(nameof(EmailSenderOptions)));
        }

        public static IServiceCollection AddHostedServices(this IServiceCollection services)
        {
            return services
                .AddHostedService<EmailSender>()
                .AddHostedService<LastTotalsMonitor>()
                .AddHostedService<SecuritiesMonitor>();
        }

        public static IServiceCollection AddOther(this IServiceCollection services)
        {
            return services
                .AddScoped<ISecuritiesParser, SecuritiesXDocumentParser>()
                .AddScoped<ITotalsParser, TotalsXDocumentParser>();
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration config, IWebHostEnvironment env)
        {
            var dbConnOptions = new DbConnectionOptions();
            config.GetSection(nameof(DbConnectionOptions)).Bind(dbConnOptions);

            var dbConnOptsBuilder = new DbContextOptionsBuilder();
            dbConnOptsBuilder.UseNpgsql(dbConnOptions.ConnectionString);

            var dbContext = new AppDbContext(dbConnOptsBuilder.Options);

            if (env.IsDevelopment())
                dbContext.Database.EnsureDeleted();

            dbContext.Database.EnsureCreated();

            return services.AddDbContext<AppDbContext>(o => o.UseNpgsql(dbConnOptions.ConnectionString));
        }

        public static IServiceCollection AddCache(this IServiceCollection services, IConfiguration config, IWebHostEnvironment env)
        {
            var options = new RedisConnectionOptions();
            config.GetSection(nameof(RedisConnectionOptions)).Bind(options);

            var multiplexer = ConnectionMultiplexer.Connect($"{options.Host},allowAdmin=true");
            var redis = multiplexer.GetServer($"{options.Host}:{options.Port}");

            if (env.IsDevelopment())
                redis.FlushAllDatabases();
            
            return services.AddSingleton<IConnectionMultiplexer>(multiplexer);
        }

        public static IServiceCollection AddUnits(this IServiceCollection services)
        {
            var typesFromAssemblies = Assembly.GetAssembly(typeof(IRepo<>))!.DefinedTypes.Where(x => x.Name.EndsWith("Unit"));

            foreach (var type in typesFromAssemblies)
                services.AddScoped(type);

            return services;
        }

        public static IServiceCollection AddRepos(this IServiceCollection services)
        {
            var typesFromAssemblies = Assembly.GetAssembly(typeof(EfRepo<>))!.DefinedTypes.Where(x => x.Name.EndsWith("Repo"));

            foreach (var type in typesFromAssemblies)
                services.Add(new ServiceDescriptor(type.GetInterface("I" + type.Name)!, type, ServiceLifetime.Scoped));

            return services;
        }
    }
}
