﻿using Microsoft.AspNetCore.Mvc;
using Monitoring.Moex.Core.Units.EmailNotifications;

namespace Monitoring.Moex.WebApi.Controllers
{
    public class EmailNotificationController : AppController
    {
        private readonly EmailSubscribersUnit _emailSubscriberUnit;

        public EmailNotificationController(EmailSubscribersUnit emailSubscriberUnit)
        {
            _emailSubscriberUnit = emailSubscriberUnit;
        }

        [HttpPost]
        public async Task Subscribe(string email)
        {
            await _emailSubscriberUnit.SubscribeAsync(email);
        }

        [HttpPost]
        public async Task Unsubscribe(string email)
        {
            await _emailSubscriberUnit.UnsubscribeAsync(email);
        }
    }
}
