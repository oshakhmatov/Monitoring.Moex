﻿using Microsoft.AspNetCore.Mvc;
using Monitoring.Moex.Core.Dto.SecurityTotals;
using Monitoring.Moex.Core.Services.SecurityTotals;

namespace Monitoring.Moex.WebApi.Controllers
{
	public class LastTotalsController : AppController
	{
		private readonly SecurityTotalsUnit _securityTotalsUnit;

        public LastTotalsController(SecurityTotalsUnit securityTotalsUnit)
        {
            _securityTotalsUnit = securityTotalsUnit;
        }

        [HttpGet]
        public async Task<LastTotalsResultDto> Get()
        {
            return await _securityTotalsUnit.GetLastTotalsAsync();
        }

        [HttpGet]
        public async Task<HighestTotalResultDto> GetHighestUp()
        {
            return await _securityTotalsUnit.GetHighestUpAsync();
        }

        [HttpGet]
        public async Task<HighestTotalResultDto> GetHighestDown()
        {
            return await _securityTotalsUnit.GetHighestDownAsync();
        }
    }
}
