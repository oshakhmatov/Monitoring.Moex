using Microsoft.AspNetCore.Mvc;

namespace Monitoring.Moex.WebApi.Controllers
{
	[ApiController]
	[Route("[controller]/[action]")]
	public class AppController : ControllerBase
	{
	}
}