using Monitoring.Moex.WebApi;
using NLog;
using NLog.Web;

var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
	var config = new ConfigurationBuilder()
		.SetBasePath(Directory.GetCurrentDirectory())
		.AddJsonFile("appsettings.json")
		.Build();

	var builder = WebApplication.CreateBuilder(args);

	builder.Logging.ClearProviders();
    builder.Host.UseNLog();

	builder.Services.AddRepos();
	builder.Services.AddUnits();
	builder.Services.AddHostedServices();
	builder.Services.AddOther();
	builder.Services.AddOptions(config);
	builder.Services.AddDatabase(config, builder.Environment);
	builder.Services.AddCache(config, builder.Environment);

	builder.Services.AddControllers();
	builder.Services.AddEndpointsApiExplorer();
	builder.Services.AddSwaggerGen();

	var app = builder.Build();

	if (app.Environment.IsDevelopment())
	{
		app.UseSwagger();
		app.UseSwaggerUI();
	}

	app.UseHttpsRedirection();
	app.UseAuthorization();
	app.MapControllers();
	app.Run();
}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    LogManager.Shutdown();
}